<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>TAGO WEB</title>
      <link rel="stylesheet" href="inicio_fan.css">
      <Script src="https://kit.fontawesome.com/887a835504.js" crossorigin="anonymous"></Script>
</head>

<body>
      <main class="main">
            <section class="contenedor-1">
                  <header class="cabecera">
                        <div class="logo">
                              <img src="imagenes/descarga-removebg-preview.png" alt="">
                        </div>
                        <nav class="navegacion">
                              <a href="#" class="link">HOME</a>
                              <a href="#" class="link">NOSOTROS</a>
                              <a href="login_dir.php" class="link">LOGIN DIRECTOR</a>
                              <a href="login_glo.php" class="link">INICIAR SESIÓN</a>
                        </nav>
                  </header>
                  <div class="banner">
                        <div class="banner_textos">
                              <h1>MATRICULATE YA</h1>
                              <P>Conoce nuestras instalaciones y nuestro plan de desarrollo academico.. </P> <br>
                              <p> Prescolar</p>
                              <p> Primaria</p>
                              <p> Educación media</p>

                        </div>
                  </div>



                  <div id="conteItemsCarrusel">
                        <div class="itemCarrusel" id="itemCarrusel-1">
                              <div class="tarjetaCarrusel" id="tarjetaCarrusel-1">
                                    <img src="imagenes/-olimpiada.png" alt="item-1-Carrousel">
                              </div>
                              <div class="flechasCarrusel">
                                    <a href="#itemCarrusel-3">
                                          <i class="fas fa-chevron-left"></i>
                                    </a>
                                    <a href="#itemCarrusel-2">
                                          <i class="fas fa-chevron-right"></i>
                                    </a>

                              </div>
                        </div>

                        <div class="itemCarrusel" id="itemCarrusel-2">
                              <div class="tarjetaCarrusel" id="tarjetaCarrusel-2">
                                    <img src="imagenes/academia.jpg" alt="item-2-carrousel">
                              </div>
                              <div class="flechasCarrusel">
                                    <a href="#itemCarrusel-1">
                                          <i class="fas fa-chevron-left"></i>
                                    </a>
                                    <a href="#itemCarrusel-3">
                                          <i class="fas fa-chevron-right"></i>
                                    </a>
                              </div>
                        </div>

                        <div class="itemCarrusel" id="itemCarrusel-3">
                              <div class="tarjetaCarrusel" id="tarjetaCarrusel-3">
                                    <img src="imagenes/academica.jpg" alt="item-3-carrosel">
                              </div>
                              <div class="flechasCarrusel">
                                    <a href="#itemCarrusel-2">
                                          <i class="fas fa-chevron-left"></i>
                                    </a>
                                    <a href="#itemCarrusel-1">
                                          <i class="fas fa-chevron-right"></i>
                                    </a>
                              </div>
                        </div>

                  </div>
      </main>

      <footer>
            <div class="datosFooter" data_name="Footer_datos">
            

            <div id="mod-custom103" class="mod-custom custom">

                  <p><strong>Dirección:</strong> Tv. 17 #25-57 – Barrio Marcella - Fusagasugá</p>


                  <p><strong>Tel:</strong> 8676132</p>


                  <p><strong>Celular:</strong> 3176492617</p>

                  <p><strong>Tel:</strong> 8674504</p>


                  <p><strong>E-mail:</strong> coleongfusa@gmail.com</p>


            </div>
            </div>
      </footer>


      </section>




</body>

</html>