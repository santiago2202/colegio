<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="selec.css">
</head>
<body>
<body>
    <section class="form-main">
        <div class="form-content">
            <div class="circle-1"></div>
            <div class="circle-2"></div>
            <div class="circle-3"></div>
            <div class="box">
                <h3>Bienvenido</h3>
                <br>
                <?php
                include("conexion_bd.php");
                include("control_log_glo.php");
                ?>
                <form action="" method="post">
                    <div class="input-box">
                        <input type="text" placeholder="Usuario" class="input-control" autocomplete="off" name="usuario">
                    </div>
                    <div class="input-box">
                        <input type="text" placeholder="¿A que rol pertenece?" class="input-control" autocomplete="off" name="rol">
                    </div>
                    <div class="input-box">
                        <input type="password" placeholder="Password" class="input-control" name="clave">
                    </div>
                    <button type="submit" class="btn" name="ingresar_glo"> Iniciar sesion</button>
                </form>
            </div>
        </div>
    </section>
</body>

    
</body>
</html>