<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="selec.css">
</head>
<body>
    <section class="form-main">
        <div class="form-content">
            <div class="circle-1"></div>
            <div class="circle-2"></div>
            <div class="circle-3"></div>
            <div class="box">
                <h3>Registro Estudiante</h3><br>
                <?php
                include("conexion_bd.php");
                include("controlador_est.php");
                ?>
                <form action="" method="post">
                  <div class="input-box">
                  <input type="text" placeholder="Nombres" class="input-control" name="nombre_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="text" placeholder="Apellidos" class="input-control" name="apellido_est" autocomplete="off">
                  <div><br>
                  <div class="input-box">
                  <input type="number" placeholder="Id Acudiente" class="input-control" name="id_acu_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="number" placeholder="Id Curso" class="input-control" name="id_curso_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="text" placeholder="¿Que rol Ocupara?" class="input-control" name="rol_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="date" placeholder="Fecha de Ingreso" class="input-control" name="fecha_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="text" placeholder="Tipo de Documento " class="input-control" name="tipo_documento_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="text" placeholder="Numero de Documento" class="input-control" name="num_doc_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="text" placeholder="usuario" class="input-control" name="usuario_est" autocomplete="off">
                  </div><br>
                  <div class="input-box">
                  <input type="paswoord" placeholder="Clave" class="input-control" name="clave_est" autocomplete="off">
                  <div><br>

                  <button type="submit" class="btn" name="regis_est"> Registrar</button>
                </form>
         
           
        </div> 
    </section>
</body>
</html>
