<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="selec.css">
</head>
<body>
    <section class="form-main">
        <div class="form-content">
            <div class="circle-1"></div>
            <div class="circle-2"></div>
            <div class="circle-3"></div>
            <div class="box">
                <h3>Registro Profesor</h3><br>
                <?php
                include("conexion_bd.php");
                include("controlador_pro.php");
                ?>
                <form action="" method="post">
                  <div class="input-box">
                  <input type="text" placeholder="Nombres" class="input-control" name="nombre_pro" autocomplete="off">

                  </div><br>
                  <div class="input-box">
                  <input type="text" placeholder="Apellidos" class="input-control" name="apellido_pro" autocomplete="off">
                  <div><br>
                  <div class="input-box">
                  <input type="text" placeholder="Num-documento" class="input-control" name="num_doc_pro" autocomplete="off">
                  <div><br>
                  <div class="input-box">
                  <input type="number" placeholder="ID del Rector" class="input-control" name="idDirectivo" autocomplete="off">
                  <div><br>

                  <button type="submit" class="btn" name="registro_pro"> Registrar</button>
                </form>
         
           
        </div> 
    </section>
</body>
</html>

